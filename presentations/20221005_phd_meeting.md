---
marp: true
---

# Overview

- Important dates
- Story paper 2
- Story paper 3

---

# Important dates

- Writing retreat : 19-21 October
- Wedding: 24-26 Feburary 2023

---

# Story paper 2
- Algnin operational and ecoligcal scale for functional diversity assessment is important because it is scale dependent
- Benefits of deriving functional traits from individual trees
    - Better insight into ecosystem functions
    - Better insight into form-function relationship
    - Unique insight into tree to tree interaction
- Main interation between trees is competiton for resources
- Competiton for solar radition is a key driver for forest structure

---
## Operational
- How to we algin operational scale to ecoligcal scale
    - Individual tree detection and delination in lidar data
    - High point density lidar allows for delination is non-dominat trees
    - Competition is about spatial neighbourhood
- Requirments of collection method
    - high enough point density + large enough spatial extent
    =ULS

---
### Aim: Assessment of competition pressure between dominant and non-dominat trees in Australian forest ecosystems
### Questions:
- How can (solar) competition between trees be assessed from ULS?
- How do the competiton traits compare to each other? Are they related or capture unique competition drivers?
- How does the competition varry between forest sites?

---
### Competition traits criteria
- needs to incorporate surrounding trees
    - hemispheric viewshed
    - solar radiation simulation
    - neighbourhood compeition

- **No**
    - Paper 1 traits
    - Nick/Peter traits
    - Alpha shapes
    - leaning distance

--- 

# Story paper 3
### Aim: Demonstration of operational workflow to identify functional diversity hotspot
### Questions:
- How can the functional diversity between sites be compared? (Beta diversity?)
- What is the range of functional diversity in Australian 
- What are the drives of functional diversity?
- Are FD results inline with other biodiversity desricptors of the study sites?


